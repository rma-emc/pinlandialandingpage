window.addEventListener("load", function(){
    document.querySelector(".show-menu").addEventListener("click", menuVisible);
    document.querySelector(".nav-panel-exit").addEventListener("click", menuVisible);
});

function menuVisible() {
    if(document.querySelector(".nav-panel.active")) {
        document.querySelector(".nav-panel").classList.remove("active");
    } else {
        document.querySelector(".nav-panel").classList.add("active");
    }
}